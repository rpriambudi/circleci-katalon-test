import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities
import com.kms.katalon.core.appium.driver.AppiumDriverManager as AppiumDriverManager
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.mobile.driver.MobileDriverType as MobileDriverType
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String browserstackUrl = 'http://rachmatpriambudi1:Swjnyvs9u5m6eTAYzN1q@hub-cloud.browserstack.com/wd/hub'

DesiredCapabilities caps = new DesiredCapabilities()

caps.setCapability('os_version', '9.0')

caps.setCapability('device', 'Google Pixel 3')

caps.setCapability('app', 'CircleCIKatalonTest')

caps.setCapability('project', 'CircleCI Katalon Test')

caps.setCapability('build', 'Build 0.0.1')

caps.setCapability('name', 'Initial Test')

AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, caps, new URL(browserstackUrl))

Mobile.verifyElementExist(findTestObject('android.widget.EditText0 - nameInputWrapper'), 10)

Mobile.closeApplication()

