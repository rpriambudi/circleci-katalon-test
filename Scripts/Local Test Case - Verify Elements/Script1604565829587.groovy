import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement

Mobile.startApplication('C:\\home\\experiment\\flutter\\circleci_katalon_test\\flutter-test.apk', true)

String xpath = '//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.widget.EditText[normalize-space(@text) = \'nameInputWrapper\']'

String textXpath = '//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[1]/android.view.View[normalize-space(@text) = \'buttonPushedText\']'

TestObject textField = new TestObject()

textField.addProperty('xpath', ConditionType.EQUALS, xpath)

AppiumDriver<?> driver = MobileDriverFactory.getDriver()

MobileElement element = driver.findElementByXPath(textXpath)

String text = element.getText()

KeywordLogger logger = new KeywordLogger()

logger.logInfo(text)

//Mobile.verifyElementExist(textField, 0)
//
//Mobile.sendKeys(textField, 'Test Tjoyyyy')

Mobile.delay(10)

//Mobile.verifyElementExist(findTestObject("android.widget.EditText0 - Fill your name"), 10);
Mobile.closeApplication()

