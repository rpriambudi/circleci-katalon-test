import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.appium.driver.AppiumDriverManager
import com.kms.katalon.core.mobile.driver.MobileDriverType
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import org.openqa.selenium.remote.DesiredCapabilities

String kobitonUrl = "https://r.rpriambudi:89c1c86e-ec6b-45c2-9342-d6bdbbfc4cc3@api.kobiton.com/wd/hub";

DesiredCapabilities caps = new DesiredCapabilities();

caps.setCapability("sessionName", "Automation test session");
caps.setCapability("sessionDescription", "");
caps.setCapability("deviceOrientation", "portrait");
caps.setCapability("captureScreenshots", true);
caps.setCapability("browserName", "chrome");
caps.setCapability("deviceGroup", "KOBITON");
caps.setCapability("deviceName", "Galaxy A5(2016)");
caps.setCapability("platformVersion", "9.0");
caps.setCapability("platformName", "Android"); 
caps.setCapability("app", "https://www.dropbox.com/s/ehtpujeshwp4di2/flutter-test.apk?dl=1");

AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, caps, new URL(kobitonUrl))

Mobile.verifyElementExist(findTestObject("android.widget.EditText0 - Fill your name"), 10);

Mobile.closeApplication();
